import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TarifasDetailComponent } from './tarifas-detail.component';

describe('TarifasDetailComponent', () => {
  let component: TarifasDetailComponent;
  let fixture: ComponentFixture<TarifasDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TarifasDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TarifasDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
