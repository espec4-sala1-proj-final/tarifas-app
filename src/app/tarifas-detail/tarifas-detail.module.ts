import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule} from '@angular/router';
import { TarifasDetailComponent } from './tarifas-detail.component';

import { FormsModule } from '@angular/forms';
import { ServicesModule } from '../services/services.module';

@NgModule({
    imports: [BrowserModule, FormsModule, 
        RouterModule.forRoot([
        { path: 'tarifa/:action/:id', component: TarifasDetailComponent}
    ])],

    declarations: [TarifasDetailComponent],
    providers: [ ServicesModule ]
})
export class TarifasDetailModule{ }
