import { Component, OnInit, Input } from '@angular/core';
import { Tax } from '../model/Tax';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { TarifasService } from '../services/tarifas.service';
import { ProdutosService } from '../services/produtos.service';
import { Product } from '../model/Product';
import { ContaContService } from '../services/contacont.service';
import { ContaContabil } from '../model/ContaContabil';

@Component({
  selector: 'app-tarifas-detail',
  templateUrl: './tarifas-detail.component.html',
  styleUrls: ['./tarifas-detail.component.scss']
})
export class TarifasDetailComponent implements OnInit {
  @Input()
  tarifa: Tax;

  public produtos: Product[];
  public contasCont: ContaContabil[];

  public produto: Product;
  public conta: ContaContabil;
  public id: number;
  public action: string;

  constructor(
    private _router: ActivatedRoute,
    private _location: Location,
    private tarifaService: TarifasService,
    private produtoService: ProdutosService,
    private contaContService: ContaContService
  ) { }

  ngOnInit(): void {
    this._router.params.subscribe((params: Params) => {
      this.produtos = this.produtoService.getMemoryProducts();
      this.contasCont = this.contaContService.getMemoryAccount();
      
      this.tarifa = new Tax();

      this.id = +params['id'];
      this.action = params['action'];

      if (this.action == 'detail') {
        this.tarifa = this.tarifaService.getMemoryTax().find(tax => tax.id == this.id);
      } else {
        this.tarifa = new Tax();
      }

      this.conta = this.contasCont.find(c => c.id == this.tarifa.contaContabilId);
      this.produto = this.produtos.find(p => p.id == this.tarifa.produtoId);
    });

  }

  public onBack() {
    this._location.back();
  }

  public save() {
    this.tarifaService.postTax(this.tarifa)
    .subscribe((_tarifa: Tax) => {
      alert("Tarifa salva com sucesso!");
    })
  }

}
