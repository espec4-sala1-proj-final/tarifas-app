const domain = true ? 'http://grupo1.spec4.mastertech.com.br/' : '/api/';

export const allTax = `${domain}tarifa/tarifas`;

export const postTax = `${domain}tarifa/tarifas`;

export const allProducts = `${domain}produto/produtos`;

export const allAccounts = `${domain}contacontabil/contabil`;

export const allCustomer = `${domain}cliente/clientes`;

export const allMovs = `${domain}movimentacao/movimentacao`;

export const customerMovs = `${domain}movimentacao/movimentacao/cliente`;

export const periodMovs = `${domain}movimentacao/movimentacao/mensal`;