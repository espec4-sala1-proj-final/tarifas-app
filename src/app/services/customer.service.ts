import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {allProducts, allCustomer} from './url';
import { Product } from '../model/Product';
import { map } from 'rxjs/operators';
import { Customer } from '../model/Customer';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  public memoryCustomers: Customer[];

  constructor(private http: HttpClient) { }

  public getAllCustomers(): Observable<Customer[]> {
    return this.http.get(allCustomer)
    .pipe(
      map((response: any) => {
        return response;
      })
    );

  }

  public postCustomer(customer: Customer): Observable<Customer> {
    return this.http.post(allCustomer, customer)
    .pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  public setMemoryCustomer(customers: Customer[]) {
    this.memoryCustomers = customers;
  }

  public getMemoryCustomer(): Customer[] {
    return this.memoryCustomers;
  }
}
