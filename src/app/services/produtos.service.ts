import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {allProducts} from './url';
import { Product } from '../model/Product';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProdutosService {
  public memoryProducts: Product[];

  constructor(private http: HttpClient) { }

  public getAllProducts(): Observable<Product[]> {
    return this.http.get(allProducts)
    .pipe(
      map((response: any) => {
        return response;
      })
    );

  }

  public setMemoryProducts(produtos: Product[]) {
    this.memoryProducts = [];
    this.memoryProducts = produtos;
  }

  public getMemoryProducts(): Product[] {
    return this.memoryProducts;
  }
}
