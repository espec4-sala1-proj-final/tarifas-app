import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { TarifasService } from './tarifas.service';
import { ProdutosService } from './produtos.service';
import { ContaContService } from './contacont.service';
import { MovimentosService } from './movimentacao.service';
import { CustomerService } from './customer.service';

const services = [
    TarifasService,
    ProdutosService,
    ContaContService,
    MovimentosService,
    CustomerService
];

@NgModule({
    imports: [
        BrowserModule,
        HttpClientModule 
    ],
    providers: [services]
})
export class ServicesModule{ }

