import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {allAccounts} from './url';
import { map } from 'rxjs/operators';
import { ContaContabil } from '../model/ContaContabil';

@Injectable({
  providedIn: 'root'
})
export class ContaContService {

  public memoryAccount: ContaContabil[];

  constructor(private http: HttpClient) { }

  public getAllAccounts(): Observable<ContaContabil[]> {
    return this.http.get(allAccounts)
    .pipe(
      map((response: any) => {
        return response;
      })
    );

  }

  public setMemoryAccount(contas: ContaContabil[]) {
    this.memoryAccount = [];
    this.memoryAccount = contas;
  }

  public getMemoryAccount(): ContaContabil[] {
    return this.memoryAccount;
  }
}
