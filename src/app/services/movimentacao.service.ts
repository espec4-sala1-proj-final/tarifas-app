import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { allMovs, periodMovs, customerMovs} from './url';
import { Observable } from 'rxjs';
import { flatMap, map } from 'rxjs/operators';
import { Movimentacao } from '../model/Movimentacao';

@Injectable({
  providedIn: 'root'
})
export class MovimentosService {

  constructor(private http: HttpClient) { }

  public memoryMovs: Movimentacao[];

  public getAllMovs(): Observable<Movimentacao[]> {
    return this.http.get(allMovs)
    .pipe(
      map((response: Movimentacao[]) => {
        return response;
      })
    );
  }

  public getMovsPeriod(month, year): Observable<Movimentacao[]> {
    return this.http.get(`${periodMovs}/${month}/${year}`)
    .pipe(
      map((response: Movimentacao[]) => {
        return response;
      })
    );
  }

  public getMovsCustomer(document): Observable<Movimentacao[]> {
    return this.http.get(`${customerMovs}/${document}`)
    .pipe(
      map((response: Movimentacao[]) => {
        return response;
      })
    );
  }

  public setMemoryMov(movimentacao: Movimentacao[]) {
    this.memoryMovs = movimentacao;
  }

  public getMemoryMov(): Movimentacao[] {
    return this.memoryMovs;
  }
}
