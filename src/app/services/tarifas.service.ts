import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { allTax, postTax } from './url';
import { Tax } from '../model/Tax'
import { Observable } from 'rxjs';
import { flatMap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TarifasService {

  constructor(private http: HttpClient) { }

  public memoryTax: Tax[];

  public getAllTax(): Observable<Tax[]> {
    return this.http.get(allTax)
    .pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  public postTax(tarifa: Tax): Observable<Tax> {
    return this.http.post(postTax, tarifa)
    .pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  public setMemoryTax(tarifas: Tax[]) {
    this.memoryTax = tarifas;
  }

  public getMemoryTax(): Tax[] {
    return this.memoryTax;
  }
}
