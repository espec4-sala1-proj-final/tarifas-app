import { Component, OnInit } from '@angular/core';
import { Customer } from '../model/Customer';
import { Location } from '@angular/common';
import { ActivatedRoute, Params } from '@angular/router';
import { CustomerService } from '../services/customer.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.scss']
})
export class CustomerDetailComponent implements OnInit {
  public id: number;
  public action: string;

  constructor(
    private _router: ActivatedRoute,
    private _location: Location,
    private customerService: CustomerService
  ) { }

  ngOnInit(): void {
   
    this._router.params.subscribe((params: Params) => {
      this.customer = new Customer();

      this.id = +params['id'];
      this.action = params['action'];

      if (this.action == 'detail') {
        this.customer = this.customerService.memoryCustomers.find(cust => cust.id == this.id);
      } else {
        this.customer = new Customer();
      }
    });
  }

  public customer: Customer;
  public levels = ["Varejo", "Uniclass", "Personalite", "Private"];

  public onBack() {
    this._location.back();
  }

  public save() {
    this.customer.cpfCnpj = this.customer.cpfCnpj.replace(/(\.|\/|\-)/g,"");
    this.customerService.postCustomer(this.customer)
    .subscribe((_tarifa: Customer) => {
      alert("Cliente salvo com sucesso!");
    })
  }

  isCPF(): boolean{
    return this.customer.cpfCnpj == null ? true : this.customer.cpfCnpj.length < 12 ? true : false;
   }
 
  getCpfCnpjMask(): string{
      return this.isCPF() ? '000.000.000-009' : '00.000.000/0000-00';
  }

}
