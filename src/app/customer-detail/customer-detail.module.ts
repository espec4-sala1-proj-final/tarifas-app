import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule} from '@angular/router';

import { CustomerDetailComponent } from './customer-detail.component';

import { FormsModule } from '@angular/forms';
import { NgxMaskModule, IConfig } from 'ngx-mask'
 
const maskConfig: Partial<IConfig> = {
  validation: false,
};

@NgModule({
    imports: [
        BrowserModule, 
        FormsModule, 
        RouterModule.forRoot([
            { path: 'customer/:action/:id', component: CustomerDetailComponent}
        ]),
        NgxMaskModule.forRoot(maskConfig)
    ],

    declarations: [CustomerDetailComponent],
    providers: []
})

export class CustomerDetailModule{ }