import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule} from '@angular/router';
import { MovimentacaoComponent } from './movimentacao.component';

import { FormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { NgxMaskModule, IConfig } from 'ngx-mask'
import { CurrencyPipe } from '@angular/common';

const maskConfig: Partial<IConfig> = {
    validation: false,
  };

  
@NgModule({
    imports: [
        BrowserModule, 
        FormsModule, 
        RouterModule.forRoot([
        { path: 'tarifasCliente/:action', component: MovimentacaoComponent}
        ]),
        BrowserAnimationsModule,
        BsDatepickerModule.forRoot(),
        NgxMaskModule.forRoot(maskConfig),
    ],
    declarations: [
        //CurrencyPipe,
        MovimentacaoComponent,
        
    ],
    providers: []
})
export class MovimentacaoModule{ }
