import { Component, OnInit, OnDestroy } from '@angular/core';
import { Movimentacao } from '../model/Movimentacao';
import { MovimentosService } from '../services/movimentacao.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-movimentacao',
  templateUrl: './movimentacao.component.html',
  styleUrls: ['./movimentacao.component.scss']
})
export class MovimentacaoComponent implements OnInit {

  public movimentacoes: Array<Movimentacao>;
  public action: string;
  public mensal: boolean;
  public cliente: boolean;
  public documento: string;
  public showAlert: boolean;

  constructor(
    private movService: MovimentosService,
    private _router: ActivatedRoute,
    private _location: Location
  ) { }

  ngOnInit(): void {
    this._router.params.subscribe((params: Params) => {
      this.showAlert = false;
      this.movimentacoes = undefined;
      this.documento = '';
      this.action = params['action'];

      this.mensal = this.action == "mensal";
      this.cliente = this.action == "cliente";

    });
  }

  onOpenCalendar(container) {
    container.monthSelectHandler = (event: any): void => {
      this.showAlert = false;
      container._store.dispatch(container._actions.select(event.date));

      let month = event.date.toJSON().toString().substring(5,7);
      let year = event.date.toJSON().toString().substring(0,4);


      this.movService.getMovsPeriod(+month, +year)
      .subscribe((_mov: Movimentacao[]) => {
        if (_mov.length == 0) {
          this.showAlert = true;
        } else {
          this.movimentacoes = new Array<Movimentacao>();
          this.movimentacoes = _mov;
        }
      });
    };     
    container.setViewMode('month');
   }

   search(event) {
    this.showAlert = false;
     if (event.key == "Enter") {
       let _document = this.documento.replace(/(\.|\/|\-)/g,"")
      this.movService.getMovsCustomer(_document)
      .subscribe((_mov: Movimentacao[]) => {
        this.movimentacoes = new Array<Movimentacao>();
        this.movimentacoes = _mov;
      }, (err) => {
        if (err && err.status) {
          this.showAlert = true;
        }
      });
     }
  }

  isCPF(): boolean{
    return this.documento == null ? true : this.documento.length < 12 ? true : false;
   }
 
  getCpfCnpjMask(): string{
      return this.isCPF() ? '000.000.000-009' : '00.000.000/0000-00';
  }

}
