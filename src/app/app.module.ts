import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CustomerModule } from './customer/customer.module';
import { TarifasModule } from './tarifas/tarifas.module';
import { WelcomeComponent } from './welcome/welcome.component';
import { RouterModule } from '@angular/router';
import { MovimentacaoModule } from './movimentacao/movimentacao.module';
import { TarifasDetailModule } from './tarifas-detail/tarifas-detail.module';
import { ServicesModule } from './services/services.module';
import { CustomerDetailModule } from './customer-detail/customer-detail.module';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CustomerModule,
    TarifasModule,
    MovimentacaoModule,
    TarifasDetailModule,
    ServicesModule,
    CustomerDetailModule,
    RouterModule.forRoot([
      { path: 'welcome', component: WelcomeComponent},
      { path: '', redirectTo:'/welcome', pathMatch:'full'},
  ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
