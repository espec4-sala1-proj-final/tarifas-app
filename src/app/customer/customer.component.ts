import { Component, OnInit } from '@angular/core';
import { Customer } from '../model/Customer';
import { CustomerService } from '../services/customer.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {

  constructor(
    private customerService: CustomerService
  ) { }

  ngOnInit(): void {
    this.customers = [];

    this.customerService.getAllCustomers()
    .subscribe((_customer: Customer[]) => {
      this.customers = _customer;
      this.customerService.setMemoryCustomer(_customer);
    })

  }

  public customers: Customer[];
}
