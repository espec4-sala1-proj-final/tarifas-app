import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule} from '@angular/router';

import { CustomerComponent } from './customer.component';

import { FormsModule } from '@angular/forms';
import { NgxMaskModule, IConfig } from 'ngx-mask'
 
const maskConfig: Partial<IConfig> = {
  validation: false,
};

@NgModule({
    imports: [
        BrowserModule, 
        FormsModule, 
        RouterModule.forRoot([
            { path: 'customer', component: CustomerComponent}
        ]),
        NgxMaskModule.forRoot(maskConfig)
    ],

    declarations: [CustomerComponent],
    providers: []
})

export class CustomerModule{ }