import { Component, OnInit } from '@angular/core';
import { Tax } from '../model/Tax';
import { TarifasService } from '../services/tarifas.service';
import { ProdutosService } from '../services/produtos.service';
import { ContaContService } from '../services/contacont.service';
import { Product } from '../model/Product';
import { ContaContabil } from '../model/ContaContabil';
import { flatMap, map } from 'rxjs/operators';

@Component({
  selector: 'app-tarifas',
  templateUrl: './tarifas.component.html',
  styleUrls: ['./tarifas.component.scss']
})
export class TarifasComponent implements OnInit {

  public tarifas: Tax[];

  public tarifa: Tax;
  public produtos: Product[];
  public contasCont: ContaContabil[];

  constructor(
    private tarifasService: TarifasService,
    private produtoService: ProdutosService,
    private contaContService: ContaContService
  ) { }

  ngOnInit(): void {
    this.tarifas = [];

    let that = this;

    this.produtoService.getAllProducts()
    .pipe(flatMap(
      (_produtos: Product[]) => {
        this.produtos = [];
        this.produtos = _produtos;
        this.produtoService.setMemoryProducts(_produtos);
        return this.contaContService.getAllAccounts();
      }
    )).pipe(
      flatMap((_contasCont: ContaContabil[]) => {
        this.contasCont = [];
        this.contasCont = _contasCont;
        this.contaContService.setMemoryAccount(_contasCont);
        return this.tarifasService.getAllTax()
      })
    ).subscribe((_tarifas: Tax[]) => {
      let teste = that.produtos
      this.tarifas = _tarifas;

      this.tarifas.forEach(tarifa => {
        tarifa.produtoDesc = this.produtos.find(produto => produto.id == tarifa.produtoId).nome;
        tarifa.contaDesc = this.contasCont.find(conta => conta.id == tarifa.contaContabilId).contaContabil;
      });

      this.tarifasService.setMemoryTax(_tarifas);
    });
  }

  public save() {
    let saveTarifa = this.tarifa;
  }

}
