import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule} from '@angular/router';
import { TarifasComponent } from './tarifas.component';

import { FormsModule } from '@angular/forms';
import { ServicesModule } from '../services/services.module';

@NgModule({
    imports: [BrowserModule, FormsModule, 
        RouterModule.forRoot([
        { path: 'manutencaoTarifas', component: TarifasComponent}
    ])],

    declarations: [TarifasComponent],
    providers: [ ServicesModule ]
})
export class TarifasModule{ }
