export class Customer {
    id: number;
    cpfCnpj: string;
    nome: string;
    segmento: string;
}