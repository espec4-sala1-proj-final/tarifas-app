export class Movimentacao {
    id: number;
    idTarifa: number;
    cpfCnpj: string;
    valorMovimento: number;
    dataMovimento: Date;
    tipo: string;
    isento: boolean;
}