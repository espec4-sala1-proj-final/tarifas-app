export class Tax {
    id: number;
    nome: string;
    produtoId: number;
    produtoDesc: string;
    contaContabilId: number;
    contaDesc: string;
}