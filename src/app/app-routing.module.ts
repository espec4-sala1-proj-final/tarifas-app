import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerModule } from './customer/customer.module';

const routes: Routes = [];

@NgModule({
  imports: [],
  exports: [RouterModule]
})
export class AppRoutingModule { }
